{
    'name': 'Normo Sale Report',
    'version': '1.0',
    'category': 'Sales',
    'summary': 'Normo Reports Sales Orders',
    'description': 'Module Nomro of Sales Orders.',
    'author': 'Your Name',
    'depends': ['sale'],
    'data': [
        'data/data.xml',
        'views/sale_order_report.xml',
        'views/sale_order.xml',
        "security/ir.model.access.csv",
    ],
    'assets': {
        'web.assets_frontend': [
            'normo/static/src/css/style.css',
        ],
    },
    'installable': True,
    'application': False,
}